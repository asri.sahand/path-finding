import pygame
import sys
import json
import heapq
import os
import codecs

DISPLAY_WIDTH = 800
DISPLAY_HEIGHT = 600

with open("jsonfiles\+000,+000,+000.json") as f:
	data = json.load(f)
firstX = data["bbox"][0]
firstY = data["bbox"][1]
secondX = data["bbox"][2]
secondY = data["bbox"][3]
scale = 1


class Node:
	def __init__(self, line1, line2, num):
		self.num = num
		self.neighbours = []
		self.distances = []
		
		# self.dis = sys.maxint
		# self.visited = False
		# self.previous = None

		l = line1.split()
		self.x = float(l[0])
		self.y = float(l[1])
		l = line2.split()
		k = int(l[0])
		for i in range(k):
			distance = float(l[2*i+2])
			self.distances.append(distance)
			neighbour = int(l[2*i+1])
			self.neighbours.append(neighbour)
nodes = []


def X(x):
	result = (int)((x-firstX)*DISPLAY_WIDTH/(secondX-firstX))
	return result
def Y(y):
	result = (int)((y-firstY)*DISPLAY_HEIGHT/(secondY-firstY))
	return result

def inverseX(x):
	result = (float)(x*(secondX - firstX)/DISPLAY_WIDTH + firstX)
	return result
def inverseY(y):
	result = (float)(y*(secondY - firstY)/DISPLAY_HEIGHT + firstY)
	return result

def draw_whole_paths():
	for n in nodes:
		for i in range(len(n.neighbours)):
			pygame.draw.line(window, (255, 255, 255),
				( X(n.x), Y(n.y) ),
				( X(nodes[n.neighbours[i]].x), Y(nodes[n.neighbours[i]].y) ) )
	pygame.display.flip()

def draw(data, x, y, scale):
	for features in data["features"]:
		if features["geometry"]["type"] == "GeometryCollection" :
			for geometries in features["geometry"]["geometries"] :
				if geometries["type"] == "LineString" :
					for i in range(len(geometries["coordinates"])):
						coordinates = geometries["coordinates"]
						if i == len(geometries["coordinates"])-1:
							break;
						pygame.draw.line(window, (255, 255, 255),
							( (X(coordinates[i][0]+x)*scale), (Y(coordinates[i][1]+y)*scale) ),
							( (X(coordinates[i+1][0]+x)*scale), (Y(coordinates[i+1][1]+y)*scale) ))

def drawZoomLevels(window, data, x, y, level):
	window.fill( (0,0,0) )
	for i in range(0,2**level):
		for j in range(0,2**level):
			fname = "jsonfiles\+00" + str(level) + ",+00" + str(i) + ",+00" + str(j) + ".json"
			if os.path.isfile(fname):
				with codecs.open(fname,'r', 'iso-8859-1') as f:
					data = json.load(f)
				draw(data, x, y, 2**level)
				pygame.display.flip()

def findNearestNode(nodes, x, y):
	minDistant = float("inf")
	nearestNode = 1
	for n in nodes:
		newDistant = (x - X(n.x))**2 + (y - Y(n.y))**2
		if newDistant < minDistant:
			minDistant = newDistant
			print(str(n.num) + ": " + str(minDistant))
			nearestNode = n
	return nearestNode

x = 0
def dijkstra(source, destination):
	global x
	Queue = []
	visited_v = []
	dis = []
	par = []

	path = []

	for i in range(n):
		# first nodes are not visited
		visited_v.append(0)
		# first weight of all nodes are Infinity
		dis.append(float("inf"))
		# first no one has parent(for path)
		# we dont have a path yet
		par.append(None)
		# Queue(weight, node)
	heapq.heappush(Queue, (0, source))
	dis[source.num] = 0
	while Queue:
		node = heapq.heappop(Queue)[1]
		# if visited:
		if (visited_v[node.num]):
			continue
		# end of dijkstra
		if (node == destination):
			break
		visited_v[node.num] = 1
		for i in range(len(node.neighbours)):
			nei = node.neighbours[i]
			#	Relax(node, v.neighbours)
			if (visited_v[nei] != 1 and dis[nei] > dis[node.num] + node.distances[i]):
				dis[nei] = dis[node.num] + node.distances[i]
				par[nei] = node
				heapq.heappush(Queue, (dis[nei], nodes[nei]))
	tmp = destination
	while (tmp):
		x += dis[tmp.num]
		path.append(tmp)
		tmp = par[tmp.num]
	path.reverse()
	print(x)
	return path

def draw_path(path, x_cam, y_cam, scale):
	for i in range(len(path)):
		if i == len(path)-1 :
			break;
		#print X(path[i].x)
		pygame.draw.line(window, (255, 0, 0),
			( (X(path[i].x + x_cam)*scale), (Y(path[i].y + y_cam)*scale) ),
			( (X(path[i+1].x + x_cam)*scale), (Y(path[i+1].y + y_cam)*scale) ) )
	pygame.display.flip()

f = open("path.data", 'r')
n = f.readline()
n.replace("\n", "")
n = int(n)

MAX_N = n

lines1 = []
for i in range(n):
	temp = f.readline()
	lines1.append(temp)
for i in range(n):
	temp = f.readline()
	p = Node(lines1[i], temp, i)
	nodes.append(p)

pygame.init()
window = pygame.display.set_mode((DISPLAY_WIDTH, DISPLAY_HEIGHT))
flag = False

zoomLevel = 0
x_cam = 0
y_cam = 0
drawZoomLevels(window, data, x_cam, y_cam, zoomLevel)
pygame.display.flip()

path = []
#scale = 1
is_path_draw = False
while True :
	for event in pygame.event.get() :
		pressed = pygame.key.get_pressed()
		mouse_pressed = pygame.mouse.get_pressed()

		if mouse_pressed[0]:
			start_pos = pygame.mouse.get_pos()
			#start_pos = inverseX(start_pos[0])
			print("start_pos = " + str(start_pos))
		elif mouse_pressed[2]:
			end_pos = pygame.mouse.get_pos()
			print("end_pos = " + str(end_pos))

		if event.type == pygame.QUIT : 
			flag = True
			break

		elif pressed[pygame.K_i] and zoomLevel < 3:
			zoomLevel += 1
		elif pressed[pygame.K_o] and zoomLevel > 0:
			zoomLevel -= 1

		if pressed[pygame.K_o] or pressed[pygame.K_i]:
			drawZoomLevels(window, data, x_cam, y_cam, zoomLevel)
			if is_path_draw:
				scale = 2**zoomLevel
				draw_path(path, x_cam, y_cam, scale)
			pygame.display.flip()

		elif pressed[pygame.K_UP]:
			y_cam += 2500
		elif pressed[pygame.K_DOWN]:
			y_cam -= 2500
		elif pressed[pygame.K_RIGHT]:
			x_cam -= 2500
		elif pressed[pygame.K_LEFT]:
			x_cam += 2500

		if 	pressed[pygame.K_UP] or pressed[pygame.K_DOWN] or pressed[pygame.K_LEFT] or pressed[pygame.K_RIGHT]:
			window.fill( (0,0,0) )
			drawZoomLevels(window, data, x_cam, y_cam, zoomLevel)
			if is_path_draw:
				scale = 2**zoomLevel
				draw_path(path, x_cam, y_cam, scale)
			pygame.display.flip()

		elif pressed[pygame.K_w] :
			window.fill( (0,0,0) )
			draw_whole_paths()
		elif pressed[pygame.K_d] :
			sourceNode = findNearestNode(nodes, start_pos[0], start_pos[1])
			destinationNode = findNearestNode(nodes, end_pos[0], end_pos[1])
			print("Source Node Num: " + str(sourceNode.num));
			print("Destination Node Num: " + str(destinationNode.num));
			
			#path = dijkstra(nodes[1], nodes[2000])
			path = dijkstra(sourceNode, destinationNode)
			for i in range(len(path)):
				if i == len(path)-1 :
					break;
				scale = 2**zoomLevel
				pygame.draw.line(window, (255, 0, 0),
					( (X(path[i].x + x_cam)*scale), (Y(path[i].y + y_cam)*scale) ),
					( (X(path[i+1].x + x_cam)*scale), (Y(path[i+1].y + y_cam)*scale) ) )
				pygame.display.flip()
				is_path_draw = True
			draw_path(path, x_cam, y_cam, scale)
			print(x_cam)
			print(y_cam)

	if flag:
		break

# Path finding	

In this project the user could choose a source and destination point in Tehran Map and the shortest path will appear.

## Getting Started

* After running the PH.py file the map would appear.
* You should Zoom In too see more details on the map.

### Button Controller

* Use the Arrow Keys to move on the map
* Use 'i' Button to Zoom In
* Use 'o' Button to Zoom Out
* Use the Left Key on the mouse to choose the source point
* Use the Right Key on the mouse to choose th destination point
* Use 'd' Button to draw the shortest path between source and destination  T  

## Built With

* [Python 2.7](https://www.python.org/download/releases/2.7/) - The Programming Language used
* [Pygame](https://www.pygame.org/) - The library used to show the map

## Author

* **Sahand Asri**